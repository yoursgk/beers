import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IBeer } from '../../models/beer/beer.interface';

@Component({
  selector: 'beerCard',
  templateUrl: './beerCard.html',
  styleUrls: ['./beerCard.css']
})
export class BeerCardComponent {
  @Output('onChange') onChangeEventEmitter: EventEmitter<IBeer>;
  @Output('onDelete') onDeleteEventEmitter: EventEmitter<IBeer>;

  @Input()
  get beer(): IBeer {
    return this._beer;
  }

  set beer(value: IBeer) {
    this._beer = value;
  }

  public isEditing: boolean;
  private _beer: IBeer;
  
  constructor(
  ) {
    this.onChangeEventEmitter = new EventEmitter();
    this.onDeleteEventEmitter = new EventEmitter();
    this.isEditing = false;
  }

  public deleteBeer(): void {
    this.onDeleteEventEmitter.emit(this.beer);
  }

  public updateBeer(): void {
    this.onChangeEventEmitter.emit(this.beer);
    this.toggleEditBeer();
  }

  public toggleEditBeer(): void {
    this.isEditing = !this.isEditing;
  }
}
