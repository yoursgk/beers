import { ApiService } from './api';
import { Beer } from '../models/beer/beer.model';
import { IBeer } from '../models/beer/beer.interface';
import { Injectable } from '@angular/core';

@Injectable()
export class BeerService {
  public beers: Array<IBeer>;

  constructor(private apiService: ApiService) { }

  public async get(page: number = 1): Promise<Array<IBeer>> {
    return this.apiService
      .get('/beers', { page: page })
      .then((response) => {
        this.beers = response.map((beer) => {
          return new Beer(beer);
        });

        return this.beers;
      });
  }
}
