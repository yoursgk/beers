import { Component, OnInit } from '@angular/core';
import { IBeer } from './models/beer/beer.interface';
import { BeerService } from './services/beer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public beers: Array<IBeer>;
  private page: number;
  private showMoreButton: boolean;
  private isLoading: boolean;

  constructor(
    private beerService: BeerService
  ) {
    this.page = 1;
    this.showMoreButton = true;
    this.isLoading = false;
  }

  public async ngOnInit(): Promise<void> {
    this.beers = await this.beerService.get();
  }

  public async getMoreBeer(): Promise<void> {
    this.isLoading = true;
    this.page++;
    let beers = await this.beerService.get(this.page);
    if (beers.length == 0) this.showMoreButton = false;
    beers.forEach((beerElement) => {
      if (this.beers.findIndex((beer) => beer.id == beerElement.id)) {
        this.beers.push(beerElement);
      }
    });
    this.isLoading = false;
  }

  public deleteBeer(beer: IBeer): void {
    this.beers.splice(this.beers.findIndex((beerElement) => beer.id == beerElement.id), 1);
  }
}
